#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <time.h>
#include <string.h>
#include <errno.h>
#include <dirent.h>
#include <signal.h>

#define FOLDER_INTERVAL 30
#define IMAGE_INTERVAL 5
#define IMAGE_COUNT 15

int is_running = 1;

char* get_current_time()
{
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);

    char* str_time = (char*) malloc(20 * sizeof(char));
    sprintf(str_time, "%04d-%02d-%02d_%02d:%02d:%02d", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);

    return str_time;
}

int create_folder(char* folder_name)
{
    printf("[%d] Create Folder process\n", getpid());
    char* folder_path = (char*) malloc(256 * sizeof(char));
    sprintf(folder_path, "./%s", folder_name);

    int status = mkdir(folder_path, 0700);
    if (status != 0)
    {
        printf("[LOG] Failed to create folder %s: %s\n", folder_path, strerror(errno));
        free(folder_name);
        free(folder_path);
        return -1;
    }

    printf("[LOG] Created folder %s\n", folder_path);

    free(folder_path);

    return 0;
}

int download_image(char* folder_name)
{
    printf("[%d] Download Image process\n", getpid());
    char* image_name = get_current_time();
    char* image_path = (char*) malloc(256 * sizeof(char));
    sprintf(image_path, "./%s/%s.jpg", folder_name, image_name);
    
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    int size = (t % 1000) + 50;
    char url[50];
    sprintf(url, "https://picsum.photos/%d/%d", size, size);

    pid_t pid = fork();

    if (pid == 0) {
        char *command = (char*) malloc(256 * sizeof(char));
        sprintf(command, "wget -q -O %s %s", image_path, url);
        printf("[%d] Wget Worker\n", getpid());
        char* args[6];
        args[0] = "sh";
        args[1] = "-c";
        args[2] = command;
        args[3] = NULL;

        execvp(args[0], args);
        printf("[LOG] Failed to execute command: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } else if (pid < 0) {
        printf("[LOG] Failed to fork process: %s\n", strerror(errno));
        free(image_name);
        free(image_path);
        return -1;
    }

    printf("[LOG] Downloaded image %s\n", image_path);

    free(image_name);
    free(image_path);

    return 0;
}


int zip_folder(char* folder_name)
{
    printf("[%d] Zip process for %s\n", getpid(), folder_name);
    char* zip_file_path = (char*) malloc(256 * sizeof(char));
    sprintf(zip_file_path, "./%s.zip", folder_name);

    pid_t pid = fork();

    if (pid == 0) {
        printf("[%d] Zip Worker\n", getpid());
        char* args[4];
        args[0] = "zip";
        args[1] = "-r";
        args[2] = zip_file_path;
        args[3] = folder_name;
        args[4] = NULL;

        execvp(args[0], args);
        printf("[LOG] Failed to execute command: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } else if (pid < 0) {
        printf("[LOG] Failed to fork process: %s\n", strerror(errno));
        free(zip_file_path);
        return -1;
    }

    int status;
    waitpid(pid, &status, 0);

    printf("[LOG] Zipped folder %s to %s\n", folder_name, zip_file_path);

    free(zip_file_path);

    return 0;
}

int remove_folder(char *folder_name)
{
    printf("[%d] Remove Folder process for %s\n", getpid(), folder_name);
    char* folder_path = (char*) malloc(256 * sizeof(char));
    sprintf(folder_path, "./%s", folder_name);

    pid_t pid = fork();

    if (pid == 0) {
        printf("[%d] Remove Folder Worker\n", getpid());
        char* args[3];
        args[0] = "rm";
        args[1] = "-rf";
        args[2] = folder_path;
        args[3] = NULL;

        execvp(args[0], args);
        printf("[LOG] Failed to execute command: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } else if (pid < 0) {
        printf("[LOG] Failed to fork process: %s\n", strerror(errno));
        free(folder_path);
        return -1;
    }

    int status;
    waitpid(pid, &status, 0);

    printf("[LOG] Removed folder %s\n", folder_path);

    free(folder_path);

    return 0;
}

int generate_program_killer(int mode, int main_pid)
{
    FILE* killer_file = fopen("killer.c", "w");
    if (killer_file == NULL)
    {
        return -1;
    }

    fprintf(killer_file, "#include <stdio.h>\n");
    fprintf(killer_file, "#include <stdlib.h>\n");
    fprintf(killer_file, "#include <unistd.h>\n");
    fprintf(killer_file, "#include <sys/types.h>\n");
    fprintf(killer_file, "#include <sys/wait.h>\n");
    fprintf(killer_file, "#include <signal.h>\n");
    fprintf(killer_file, "#include <string.h>\n");
    fprintf(killer_file, "#include <errno.h>\n");
    fprintf(killer_file, "\n");
    fprintf(killer_file, "int kill_program()\n");
    fprintf(killer_file, "{\n");
    fprintf(killer_file, "    pid_t pid = fork();\n");
    fprintf(killer_file, "    if (pid == 0)\n");
    fprintf(killer_file, "    {\n");
    fprintf(killer_file, "        char* args[4];\n");
    fprintf(killer_file, "        args[0] = \"sh\";\n");
    fprintf(killer_file, "        args[1] = \"-c\";\n");
    if (mode == 1)
        fprintf(killer_file, "        args[2] = \"ps -e | grep lukisan | awk '{print $1}' | xargs kill -9; rm -rf killer\";\n");
    else if (mode == 2)
        fprintf(killer_file, "        args[2] = \"ps -e | grep lukisan | awk '{print $1}' | xargs kill -3; rm -rf killer\";\n");
    fprintf(killer_file, "        args[3] = NULL;\n");
    fprintf(killer_file, "\n");
    fprintf(killer_file, "        execvp(args[0], args);\n");
    fprintf(killer_file, "        printf(\"Failed to execute command: %%s\\n\", strerror(errno));\n");
    fprintf(killer_file, "        exit(EXIT_FAILURE);\n");
    fprintf(killer_file, "    }\n");
    fprintf(killer_file, "    else if (pid < 0)\n");
    fprintf(killer_file, "    {\n");
    fprintf(killer_file, "        printf(\"Failed to fork process: %%s\\n\", strerror(errno));\n");
    fprintf(killer_file, "        return -1;\n");
    fprintf(killer_file, "    }\n");
    fprintf(killer_file, "\n");
    fprintf(killer_file, "    int status;\n");
    fprintf(killer_file, "    waitpid(pid, &status, 0);\n");
    fprintf(killer_file, "\n");
    fprintf(killer_file, "    return 0;\n");
    fprintf(killer_file, "}\n");
    fprintf(killer_file, "\n");
    fprintf(killer_file, "int main()\n");
    fprintf(killer_file, "{\n");
    fprintf(killer_file, "if (kill_program() == 0)\n");
    fprintf(killer_file, "    printf(\"Killed program\\n\");\n");
    fprintf(killer_file, "else\n");
    fprintf(killer_file, "    printf(\"Failed to kill program\\n\");\n");
    fprintf(killer_file, "\n");
    fprintf(killer_file, "    return 0;\n");
    fprintf(killer_file, "}\n");

    fclose(killer_file);

    pid_t pid = fork();

    if (pid == 0)
    {
        char* args[4];
        args[0] = "sh";
        args[1] = "-c";
        args[2] = "gcc -o killer killer.c; rm killer.c";
        args[3] = NULL;

        execvp(args[0], args);
        printf("Failed to execute command: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    } else if (pid < 0) {
        printf("Failed to fork process: %s\n", strerror(errno));
        return -1;
    }

    int status;
    waitpid(pid, &status, 0);

    // printf("Generated program killer\n");

    return 0;
}

void stop(int signum)
{
    printf("Received signal %d from program killer\n", signum);
    is_running = 0;
}

int main(int argc, char* argv[])
{
    int i, mode;
    pid_t pid, pid2, sid;
    
    // Mode handler
    if (argv[1] == NULL)
    {
        printf("No mode specified\n");
        exit(EXIT_FAILURE);
    }

    if (strcmp(argv[1], "-a") == 0)
    {
        mode = 1;
    } else if (strcmp(argv[1], "-b") == 0) {
        mode = 2;
    } else {
        printf("Invalid mode\n");
        exit(EXIT_FAILURE);
    }
    

    // Register signal handler
    signal(SIGQUIT, stop);
    
    if (generate_program_killer(mode, getpid()) != 0)
    {
        exit(EXIT_FAILURE);
    }

    pid = fork();

    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();

    if (sid < 0) {
        printf("Failed to create session: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    if ((chdir("/home/index/sisop/Modul_2/soal2")) < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    
    while (is_running)
    {
        pid2 = fork();

        if (pid2 == 0)
        {
            printf("[%d] Loop process %d\n", getpid(), i);
            char* folder_name = get_current_time();
            if (create_folder(folder_name) != 0)
            {
                exit(EXIT_FAILURE);
            }

            for (i = 0; i < IMAGE_COUNT; i++)
            {
                if (download_image(folder_name) != 0)
                {
                    exit(EXIT_FAILURE);
                }

                sleep(IMAGE_INTERVAL);
            }

            if (zip_folder(folder_name) != 0)
            {
                exit(EXIT_FAILURE);
            }

            if (remove_folder(folder_name) != 0)
            {
                exit(EXIT_FAILURE);
            }

            free(folder_name);
            exit(EXIT_SUCCESS);
        }
        else if (pid2 < 0)
        {
            printf("Failed to fork process: %s\n", strerror(errno));
        }

        sleep(FOLDER_INTERVAL);
    }

    return 0;
}
