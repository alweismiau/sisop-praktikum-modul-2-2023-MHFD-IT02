#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <time.h>
#include <string.h>
#include <errno.h>
#include <dirent.h>
#include <signal.h>

#define ZIP_FILE "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download"
#define HOME_PATH "/home/index/sisop/Modul_2/soal3/"

int download_zip()
{
    pid_t pid = fork();

    if (pid == 0) {
        execlp("wget", "wget", "-q", ZIP_FILE, "-O", "players.zip", NULL);
        printf("[%d::ERRROR] Failed to execute wget", getpid());
    } else if (pid < 0) {
        printf("[LOG] Failed to fork process: %s\n", strerror(errno));
        return -1;
    }

    int status;
    waitpid(pid, &status, 0);

    printf("[LOG] wget exited with status %d\n", status);
    return 0;
}

int extract_zip()
{
    pid_t pid = fork();

    if (pid == 0) {
        execlp("unzip", "unzip", "-q", "players.zip", NULL);
        printf("[%d::ERRROR] Failed to execute unzip", getpid());
    } else if (pid < 0) {
        printf("[LOG] Failed to fork process: %s\n", strerror(errno));
        return -1;
    }

    int status;
    waitpid(pid, &status, 0);

    printf("[LOG] unzip exited with status %d\n", status);
    return 0;
}

int remove_zip()
{
    pid_t pid = fork();

    if (pid == 0) {
        execlp("rm", "rm", "-f", "players.zip", NULL);
        printf("[%d::ERRROR] Failed to execute rm", getpid());
    } else if (pid < 0) {
        printf("[LOG] Failed to fork process: %s\n", strerror(errno));
        return -1;
    }

    int status;
    waitpid(pid, &status, 0);

    printf("[LOG] rm exited with status %d\n", status);
    return 0;
}

int only_ManUtd()
{
    pid_t pid = fork();

    if (pid == 0) {
        execlp("find", "find", "players", "-type", "f", "!", "-name", "*ManUtd*", "-exec", "rm", "{}", ";", (char *) NULL);
    } else if (pid < 0) {
        printf("[LOG] Failed to fork process: %s\n", strerror(errno));
        return -1;
    }

    int status;
    waitpid(pid, &status, 0);

    printf("[LOG] find exited with status %d\n", status);
    return 0;
}

int filter()
{
    pid_t pid = fork();

    if (pid == 0) {
        execlp("mkdir", "mkdir", "-p", "players/Kiper", "players/Bek", "players/Gelandang", "players/Penyerang", NULL);
    } else if (pid < 0) {
        printf("[LOG] Failed to fork process: %s\n", strerror(errno));
        return -1;
    }

    int status;
    waitpid(pid, &status, 0);

    printf("[LOG] mkdir exited with status %d\n", status);

    char *position[] = {"Kiper", "Bek", "Gelandang", "Penyerang"};

    for (int i = 0; i < 4; i++) {
        pid = fork();

        if (pid < 0) {
            printf("[LOG] Failed to fork process: %s\n", strerror(errno));
            return -1;
        } else if (pid == 0) {
            char pattern[20];
            char dest[20];
            sprintf(pattern, "*%s*", position[i]);
            sprintf(dest, "players/%s", position[i]);
            printf("[LOG] pattern: %s, dest: %s\n", pattern, dest);
            execlp("find", "find", "players", "-maxdepth", "1", "-type", "f", "-name", pattern, "-exec", "mv", "{}", dest, ";", (char *) NULL);
        }
    }
    pid_t wpid;
    while ((wpid = wait(NULL)) != -1);
    return 0;
}

int buatTim(int bek, int gelandang, int striker)
{
    char *position[] = {"Kiper", "Bek", "Gelandang", "Penyerang"};

    for (int i = 0; i < 4; i++) {
        pid_t pid = fork();

        if (pid < 0) {
            printf("[LOG] Failed to fork process: %s\n", strerror(errno));
            return -1;
        } else if (pid == 0) {
            char namaFile[30];
            char command[150];
            sprintf(namaFile, "/home/index/Formasi_%d_%d_%d.txt", bek, gelandang, striker);
            
            if (i == 0) {
                sprintf(command, "find players/%s -type f | sort -t _ -k 4 -n | tail -n %d | awk -F/ '{print $3}'|awk -F. '{print $1}'>> %s", position[i], 1, namaFile);
            } 
            else if (i == 1) {
                sprintf(command, "find players/%s -type f | sort -t _ -k 4 -n | tail -n %d | awk -F/ '{print $3}'|awk -F. '{print $1}'>> %s", position[i], bek, namaFile);
                
            }
            else if (i == 2) {
                sprintf(command, "find players/%s -type f | sort -t _ -k 4 -n | tail -n %d | awk -F/ '{print $3}'|awk -F. '{print $1}'>> %s", position[i], gelandang, namaFile);
            }
            else if (i == 3) {
                sprintf(command, "find players/%s -type f | sort -t _ -k 4 -n | tail -n %d | awk -F/ '{print $3}'|awk -F. '{print $1}'>> %s", position[i], striker, namaFile);
            }
            
            execlp("sh", "sh", "-c", command, (char *) NULL);
        }
    }
    pid_t wpid;
    while ((wpid = wait(NULL)) != -1);
    return 0;
}

int main()
{
    if(download_zip() != 0) {
        printf("[LOG] Failed to download zip file");
        exit(EXIT_FAILURE);
    }

    if(extract_zip() != 0) {
        printf("[LOG] Failed to extract zip file");
        exit(EXIT_FAILURE);
    }

    if(remove_zip() != 0) {
        printf("[LOG] Failed to remove zip file");
        exit(EXIT_FAILURE);
    }

    if(only_ManUtd() != 0) {
        printf("[LOG] Failed to remove non-ManUtd files");
        exit(EXIT_FAILURE);
    }

    if(filter() != 0) {
        printf("[LOG] Failed to filter files");
        exit(EXIT_FAILURE);
    }

    if(buatTim(4, 4, 2) != 0) {
        printf("[LOG] Failed to make a team");
        exit(EXIT_FAILURE);
    }

    return 0;
}
