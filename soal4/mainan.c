#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

int main(int argc, char *argv[]) {
    // Parse command line arguments
    if (argc != 5) {
        printf("Usage: %s <hour> <minute> <second> <command>\n", argv[0]);
        return 1;
    }

    int hour, minute, second;

    // Handle wildcard values
    if (strcmp(argv[1], "*") == 0) {
        hour = -1;
    } else {
        hour = atoi(argv[1]);
        if (hour < 0 || hour > 23) {
            printf("Invalid hour value.\n");
            return 1;
        }
    }

    if (strcmp(argv[2], "*") == 0) {
        minute = -1;
    } else {
        minute = atoi(argv[2]);
        if (minute < 0 || minute > 59) {
            printf("Invalid minute value.\n");
            return 1;
        }
    }

    if (strcmp(argv[3], "*") == 0) {
        second = -1;
    } else {
        second = atoi(argv[3]);
        if (second < 0 || second > 59) {
            printf("Invalid second value.\n");
            return 1;
        }
    }

    char *command = argv[4];
    char *tmp;
    strcpy(tmp, command);
    char *tok = strtok(tmp, " ");

    // Check if command exists
    if (access(tok, F_OK) == -1) {
        printf("Command not found.\n");
        printf("Make sure to use absolute path.\n");
        return 1;
    }

    pid_t pid, sid;

    pid = fork();

    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    if ((chdir("/")) < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
    
    while (1) {
        // Get current time
        time_t now = time(NULL);
        struct tm *tm_now = localtime(&now);

        // Check if current time matches input values
        if ((hour == -1 || tm_now->tm_hour == hour) &&
            (minute == -1 || tm_now->tm_min == minute) &&
            (second == -1 || tm_now->tm_sec == second)) {
            // Run command
            pid_t pid2 = fork();

            if (pid2 < 0) {
                exit(EXIT_FAILURE);
            } else if (pid2 == 0) {
                char *args[] = {"bash", "-c", command, NULL};
                execv("/bin/bash", args);
            }

            // Sleep for 1 second to avoid running the command multiple times
            sleep(1);
        }
    }

    return 0;
}
