#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/wait.h>
#include <curl/curl.h>
#include <sys/types.h>

#define MAX_PATH_LEN 1024
#define MAX_FILE_COUNT 1024

int main()
{
    // Download zip file
    CURL *curl;
    FILE *fp;
    CURLcode res;
    //Link Gdrive
    char *url = "https://drive.google.com/uc?id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq&export=download";
    char outfilename[] = "binatang.zip";

    curl = curl_easy_init();
    if (curl)
    {
        fp = fopen(outfilename, "wb");
        curl_easy_setopt(curl, CURLOPT_URL, url);
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
        res = curl_easy_perform(curl);
        curl_easy_cleanup(curl);
        fclose(fp);
    }

    // Check download
    fp = fopen(outfilename, "rb");
    if (fp)
    {
        printf("File berhasil di download :D.\n");
        fclose(fp);
    }
    else
    {
        printf("Gagal mendownload file :(.\n");
        exit(1); // terminate program ketika gagal
    }

    // Unzip file
    pid_t pid = fork();
    if (pid == 0)
    {
        // child process
        execl("/usr/bin/unzip", "unzip", "binatang.zip", NULL); 
    }
    else if (pid > 0)
    {
        // parent process
        wait(NULL);
        printf("Unzipping selesai ^^.\n");
    }
    else
    {
        // fork failed
        printf("Fork gagal :(.\n");
        exit(1);
    }

    // Random file selection
    srand(time(NULL));
    DIR *dir;
    struct dirent *ent;
    char path[MAX_PATH_LEN];

    // Membuat array untuk store filename
    char files[MAX_FILE_COUNT][MAX_PATH_LEN];
    int file_count = 0;

    // Buka folder dan read
    if ((dir = opendir(".")) != NULL)
    {
        while ((ent = readdir(dir)) != NULL)
        {
            // filter JPG files
            if (ent->d_type == DT_REG && strstr(ent->d_name, ".jpg") != NULL)
            {
                snprintf(files[file_count], MAX_PATH_LEN, "%s", ent->d_name);
                file_count++;
            }
        }
        closedir(dir);
    }

    // Pilih random file
    if (file_count > 0)
    {
        int rand_index = rand() % file_count;
        printf("Grape-kun melakukan shift penjagaan pada hewan: %s\n", files[rand_index]);
    }
    else
    {
        printf("Tidak ada file JPG :(.\n");
        exit(1); // terminate program
    }

    // Move files to appropriate folders

    // Hewan air
    pid = fork();
    if (pid == 0)
    {
        // Child process
        execl("/bin/mkdir", "mkdir", "HewanAir", NULL);
        exit(EXIT_SUCCESS);
    }
    else
    {
        // Parent process
        wait(NULL);

        //Hewan Darat
        pid = fork();
        if (pid == 0)
        {
            // Child process
            execl("/bin/mkdir", "mkdir", "HewanDarat", NULL);
            exit(EXIT_SUCCESS);
        }
        else
        {
            // Parent process
            wait(NULL);
            
            // Hewan Amphibi
            pid = fork();
            if (pid == 0)
            {
                // Child process
                execl("/bin/mkdir", "mkdir", "HewanAmphibi", NULL);
                exit(EXIT_SUCCESS);
            }
            else
            {
                // Parent process
                wait(NULL);

                // Move to folder
                pid = fork();
                if (pid == 0)
                {
                    execl("/bin/bash", "bash", "-c", "mv *_air.jpg ./HewanAir; mv *_darat.jpg ./HewanDarat; mv *_amphibi.jpg ./HewanAmphibi", NULL);
                }
                else
                {
                    wait(NULL);
                }
            }
        }
    }

    // Zip
    pid = fork();
    if (pid == 0)
    {
        // child process
        execl("/usr/bin/zip", "zip", "-r", "HewanAir.zip", "HewanAir", NULL);
        exit(EXIT_FAILURE);
    }
    else if (pid < 0)
    {
        // error
        perror("fork failed");
        exit(EXIT_FAILURE);
    }
    else
    {
        // parent process
        wait(NULL);
        pid = fork();
        if (pid == 0)
        {
            // child process
            execl("/usr/bin/zip", "zip", "-r", "HewanDarat.zip", "HewanDarat", NULL);
            exit(EXIT_FAILURE);
        }
        else if (pid < 0)
        {
            // error
            perror("fork failed");
            exit(EXIT_FAILURE);
        }
        else
        {
            // parent process
            wait(NULL);
            pid = fork();
            if (pid == 0)
            {
                // child process
                execl("/usr/bin/zip", "zip", "-r", "HewanAmphibi.zip", "HewanAmphibi", NULL);
                exit(EXIT_FAILURE);
            }
            else if (pid < 0)
            {
                // error
                perror("fork failed");
                exit(EXIT_FAILURE);
            }
            else
            {
                // parent process
                wait(NULL);
                // remove directories

                execl("/bin/rm", "rm", "-r", "HewanAir", "HewanDarat", "HewanAmphibi", NULL);
                perror("exec failed");
            }
        }
    }

    return 0;
}
